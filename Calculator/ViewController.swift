//
//  ViewController.swift
//  Calculator
//
//  Created by Farakh Salman Bhatti on 14/08/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}
class CalculatorFunctions{
    @discardableResult
    func add(paramA: Int, paramB:Int) -> Int {
        
        return paramA + paramB
    }
    func subtract(paramA: Int, paramB:Int) -> Int {
        
        return paramA - paramB
    }
    func multiply(paramA: Int, paramB:Int) -> Int {
        
        return paramA * paramB
    }
    func divide(paramA: Int, paramB:Int) -> Int {
        guard paramB != 0 else {
            return -1
        }
        return paramA / paramB
    }
}

