//
//  CalculatorTests.swift
//  CalculatorTests
//
//  Created by Farakh Salman Bhatti on 14/08/2021.
//

import XCTest
@testable import Calculator



class CalculatorTests: XCTestCase {
    var sut: CalculatorFunctions!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        sut = CalculatorFunctions()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        try super.tearDownWithError()
    }
    func testAddition() {
      // given
      let valA =  5
      let valB =  5

      // when
       let result =  sut.add(paramA: valA, paramB: valB)
        
      // then
      XCTAssertEqual(result, 10, "Addition opertation failed.")
    }
    func testmultiplication() {
      // given
      let valA =  5
      let valB =  5

      // when
       let result =  sut.multiply(paramA: valA, paramB: valB)
        
      // then
      XCTAssertEqual(result, 25, "Multiplication opertation failed.")
    }
}
